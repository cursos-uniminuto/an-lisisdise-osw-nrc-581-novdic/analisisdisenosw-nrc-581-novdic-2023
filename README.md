
Creé el ReadMe...


<h1>Command line instructions</h1>

You can also upload existing files from your computer using the instructions below.

Git global setup

git config --global user.name "Jahir Saavedra" <br>
git config --global user.email "jahir.saavedra@uniminuto.edu" <br>


Create a new repository

git clone https://gitlab.com/cursos-uniminuto/an-lisisdise-osw-nrc-581-novdic/analisisdisenosw-nrc-581-novdic-2023.git <br>
cd analisisdisenosw-nrc-581-novdic-2023 <br>
git switch --create main <br>
touch README.md <br>
git add README.md <br>
git commit -m "add README" <br>
git push --set-upstream origin main <br>

Push an existing folder

cd existing_folder <br>
git init --initial-branch=main <br>
git remote add origin https://gitlab.com/cursos-uniminuto/an-lisisdise-osw-nrc-581-novdic/analisisdisenosw-nrc-581-novdic-2023.git <br>
git add . <br>
git commit -m "Initial commit" <br>
git push --set-upstream origin main <br>


Push an existing Git repository

cd existing_repo <br>
git remote rename origin old-origin <br>
git remote add origin https://gitlab.com/cursos-uniminuto/an-lisisdise-osw-nrc-581-novdic/analisisdisenosw-nrc-581-novdic-2023.git <br>
git push --set-upstream origin --all <br>
git push --set-upstream origin --tags <br>